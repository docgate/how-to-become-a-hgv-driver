If you like driving, you must know about lorry drivers. But before talking about how to become an HGV driver?  HGV is commonly referred to as a lorry driver who runs Heavy Goods Vehicles of more than 3500KG. This article is for those who Want to start their career in the HGV or haulage industry. If you have a deep interest in learning, you must continue reading this article.  
  
The basic requirements are simple to become a lorry driver in the UK. For this, you must be 18+, have a UK car license but If you don’t have a UK car license first thing you should do is to transfer a non-UK car license to the UK.  
 
This job is not for those who want office/desk type jobs. 
 
**What type of HGV license should I get?**
 
All the vehicles have different license requirements regarding their weights. Depending on the weight and additional weight when they are connected to a trailer. 
 
Types of HGV Licenses: 

**Cat C1+E** = 3,500 kg to 7,500 kg + trailer of 750 kg 

**Cat C1=** 3,500 and 7,500kg with or without trailer of 750kg weight. 
 
**Cat C drives** larger vehicles weighing over 3,500kg, and pulls a trailer weighing up to 750kg. 
 
**Cat C+E** drive weighs over 3,500kg, but with a trailer which will weigh quite 750kg, and that’s what you'll drive with a Cat C+E licence. 
 
**Why Should I Take HGV training?**
 
Making investment in HGV training allows you to secure your future employment. Once you get trained and be able to get a HGV license you will be able to get job chances not only in the UK but also across Europe. 
Suppose All Europe countries need to transport their goods and they trained HGV drivers; you can be one of the drivers who can inculcate your professional role. You will fill your desired position. A survey says that the UK [is short of 45,000 HGV drivers]( https://www.thesun.co.uk/news/5335306/trucker-shortage-threatens-post-brexit-trade-boom-with-45000-jobs-to-fill/). 
 
**The Steps to Secure HGV license to become an HGV Driver**
 
Steps to get an HGV license are clear cut and well defined. You need just right support and proper training. 
 
**Medical test for GHV Drivers** 
 
Every driver must pass the medical exam to prove him free from any medical problem. You don’t have any medical Condition that could prevent you from safe driving. Medical examiner takes conversation with you as well as a brief medical examination. Any qualified doctor can conduct this test and you will fill the can [DVLA form](https://www.gov.uk/reapply-driving-licence-medical-condition). The HGVT team will ensure that you must be booked with an approved GP nearby you. 

**Theory Test for HGV Drivers**

This test checks your driving training and consists of multiple questions and you will need to take this test in an official test center. The training company will conduct many mock exams before the official test. You will get enough training and be able to clear this test. 
 
**HGV practical training** 

Once you will pass the tests and get passing results from DVSA it'll be the time when you will get a vehicle and start training. During this training, DVSA certified and experienced instructors will train you with real vehicles. With the experience of an instructor, you will be able to pass the practical test. Once you are ready to pass for the first time, you will be booked. 
 
**Driver CPC training** 
 
If you pass the practical training test, you need to pass the CPC driving training to drive as a professional. If you don’t have a license. Driver CPC is referred to as Certificate of Professional Competence and it has standards of driving vehicles for living. 
 
**How to know if HGV driving is for you** 
 
If you love driving, want to explore Europe, HGV could be for you. The professional HGV drivers are patient and focused. Well trained and sharp at record keeping to prevent any inconvenience. They have the ability to track all the problems and their solutions on time. If you have all these qualities, you can [become an HGV driver](https://www.hgvt.co.uk/how-to-become-a-lorry-driver-in-the-uk/). 
